﻿using SharpSenses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoInovacaoINDT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Create a PXCMSenseManager instance

            PXCMSenseManager sm = PXCMSenseManager.CreateInstance();

            // Select the color stream

            sm.EnableStream(PXCMCapture.StreamType.STREAM_TYPE_COLOR, 640, 480);



            // Initialize and Stream samples

            sm.Init();

            for (; ; )
            {

                // This function blocks until a color sample is ready

                if (sm.AcquireFrame(true) < pxcmStatus.PXCM_STATUS_NO_ERROR) break;



                // retrieve the sample

                PXCMCapture.Sample sample = sm.QuerySample();



                // work on the image sample.color


                // go fetching the next sample

                sm.ReleaseFrame();

            }



            // Close down

            sm.Dispose();


        }
    }
}
